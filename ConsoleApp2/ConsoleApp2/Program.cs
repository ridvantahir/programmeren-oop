﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Stoel stoel01 = new Stoel();

            stoel01.aantalPooten = 4;
            stoel01.merk = "ikea";

            Console.WriteLine($"de gegevens van de stoel: aantal poten: {stoel01.aantalPooten}, merk: {stoel01.merk}");
            
        }
    }
}
