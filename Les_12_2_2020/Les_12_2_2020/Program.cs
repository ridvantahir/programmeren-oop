﻿using System;
using System.Collections;

namespace Les_12_2_2020
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            DateTime currentTime = DateTime.Now;
            Console.WriteLine(currentTime);

            DateTime today = DateTime.Today;
            DateTime borodinoBattle = new DateTime(1812, 9, 7);

            TimeSpan diff = today - borodinoBattle;

            Console.WriteLine($"{diff.TotalDays} days have passed since the Battle of Borodino.");

            DateTime now = DateTime.Now;

            Console.WriteLine("geen u verjaardag in");

            DateTime birthDay = DateTime.Parse(Console.ReadLine());

            DateTime nextBirthday = new DateTime(now.Year, birthDay.Month, birthDay.Day);

            if (nextBirthday == now)
            {
                nextBirthday = nextBirthday.AddYears(1);
            }

            int days = (nextBirthday - now).Days;

            Console.WriteLine($"Nog {days} dagen tot je verjaardag.");

            Console.WriteLine(now.ToString("dd/MM/yyyy HH:mm"));
        }
    }
}
