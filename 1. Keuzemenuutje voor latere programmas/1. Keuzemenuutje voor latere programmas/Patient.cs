﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _1._Keuzemenuutje_voor_latere_programmas
{
    class Patient
    {
        public string PatientName { get; set; }
        public string PatientGender { get; set; }
        public string ParientLifestyle { get; set; }
        public int PatientDays { get; set; }
        public int PatientMonths { get; set; }
        public int PatientYears { get; set; }
    }
}
