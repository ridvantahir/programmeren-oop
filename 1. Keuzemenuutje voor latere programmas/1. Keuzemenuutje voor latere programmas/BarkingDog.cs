﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _1._Keuzemenuutje_voor_latere_programmas
{
    class BarkingDog
    {
        public string Name;
        public string Breed;
        Random rng = new Random();

        public BarkingDog()
        {
            // bepaal hier met de randomgenerator het ras van de hond
            // stel hier `Breed` in
            if (rng.Next(0, 3) == 0)
            {
                Breed = "German Shepherd";
            }
            else if (rng.Next(0, 3) == 1)
            {
                Breed = "Wolfspitz";
            }
            else
            {
                Breed = "Chihuahua";
            }
            // maak van `Breed` een eigenschap zoals we dat bij de geometrische vormen hebben gedaan
        }
        public string Bark()
        {
            if (Breed == "German Shepherd")
            {
                return "RUFF!";
            }
            else if (Breed == "Wolfspitz")
            {
                return "AwawaWAF!";
            }
            else if (Breed == "Chihuahua")
            {
                return "ARF ARF ARF!";
            }
            // dit zou nooit mogen gebeuren
            // maar als de programmeur van Main iets fout doet, kan het wel
            else
            {
                return "Euhhh... Miauw?";
            }
        }
    }
}
