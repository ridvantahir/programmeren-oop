﻿using System;

namespace _1._Keuzemenuutje_voor_latere_programmas
{
    class Program
    {
        static void Main(string[] args)
        {
            //int input = 0;
            bool stopper = false;
            bool stopper02 = false;
            int width = 0;
            int heigth = 0;
            int patientCount = 0;
            ShapesBuilder builder = new ShapesBuilder();
            Car car01 = new Car();
            Car car02 = new Car();

            while (!stopper)
            {
                Console.WriteLine($"Wat wil je doen?\n1. Vormen Tegenen (h8-vormen)\n2. Auto's laten rijden (h8-autos)\n3. Patienten tonen (h8-patienten)\n4. Honden laten blaffen (h8-blaffende honden)");

                switch (int.Parse(Console.ReadLine()))
                {
                    case 1:
                        Console.WriteLine("Wat wil je tekenen?\n\t1. Rechthoek\n\t2. Driehoek");
                        switch (int.Parse(Console.ReadLine()))
                        {
                            // om een rechthoek te tekenen
                            case 1:
                                Console.WriteLine("Geef de hoogte in");
                                heigth = int.Parse(Console.ReadLine());
                                Console.WriteLine("Geef de breedte in");
                                width = int.Parse(Console.ReadLine());
                                builder.Rectangle(heigth,width);
                                break;
                                // om een driehoek te tekenen
                            case 2:
                                Console.WriteLine("Geef de hoogte in");
                                heigth = int.Parse(Console.ReadLine());
                                Console.WriteLine("Geef de breedte in");
                                width = int.Parse(Console.ReadLine());
                                builder.Triangle(heigth, width);
                                break;
                            default:
                                break;
                        }
                        break;
                    case 2:
                        for (int i = 0; i < 5; i++)
                        {
                            // om de erste auto te versnellen
                            car01.Gas();
                            Console.WriteLine($"Auto 1: {car01.Speed}km/u, Afgelegde weg {car01.Odometer:f2}km.");

                            System.Threading.Thread.Sleep(2000);
                        }
                        for (int i = 0; i < 3; i++)
                        {
                            // om de eerste auto te verstragen
                            car01.Brake();
                            Console.WriteLine($"Auto 1: {car01.Speed}km/u, Afgelegde weg {car01.Odometer:f2}km.");

                            System.Threading.Thread.Sleep(2000);
                        }
                        for (int i = 0; i < 10; i++)
                        {
                            // om de tweede auto te versnellen
                            car02.Gas();
                            Console.WriteLine($"Auto 2: {car02.Speed}km/u, Afgelegde weg {car02.Odometer:f2}km.");

                            System.Threading.Thread.Sleep(2000);
                        }
                        for (int i = 0; i < 1; i++)
                        {
                            // om de tweede auto te verstragen
                            car02.Brake();
                            Console.WriteLine($"Auto 2: {car02.Speed}km/u, Afgelegde weg {car02.Odometer:f2}km.");

                            System.Threading.Thread.Sleep(2000);
                        }

                        System.Threading.Thread.Sleep(10000);
                        break;
                    case 3:
                        Console.WriteLine("Hoe veel parienten zijn er?");
                        patientCount = int.Parse(Console.ReadLine());
                        //patienten object array declareren
                        Patient[] patients = new Patient[patientCount];
                        //patienten properties invullen
                        for (int i = 0; i < patientCount; i++)
                        {
                            Patient patient = new Patient();

                            Console.WriteLine("Wat is de naam van de patient");
                            patient.PatientName = Console.ReadLine();

                            Console.WriteLine("Wat is de geslacht van de patient");
                            patient.PatientGender = Console.ReadLine();

                            Console.WriteLine("Wat is de levenstijl van de patient");
                            patient.ParientLifestyle = Console.ReadLine();

                            Console.WriteLine("Wat is de geboorte dag van de patient");
                            patient.PatientDays = int.Parse(Console.ReadLine());

                            Console.WriteLine("Wat is de geboorte maand van de patient");
                            patient.PatientMonths = int.Parse(Console.ReadLine());

                            Console.WriteLine("Wat is de geboorte jaar van de patient");
                            patient.PatientYears = int.Parse(Console.ReadLine());

                            patients[i] = patient;
                        }
                        //patienten info afprinten
                        for (int i = 0; i < patientCount; i++)
                        {
                            Console.WriteLine($"{patients[i].PatientName} ({patients[i].PatientGender}, {patients[i].ParientLifestyle}), geboren {patients[i].PatientDays}-{patients[i].PatientMonths}-{patients[i].PatientYears}.");
                        }
                        System.Threading.Thread.Sleep(10000);
                        break;
                    case 4:
                        BarkingDog dog1 = new BarkingDog();
                        BarkingDog dog2 = new BarkingDog();
                        dog1.Name = "Swieber";
                        dog2.Name = "Misty";
                        while (!stopper02)
                        {
                            Console.WriteLine(dog1.Bark());
                            System.Threading.Thread.Sleep(1000);
                            Console.WriteLine(dog2.Bark());
                            System.Threading.Thread.Sleep(1000);
                            Console.Clear();
                            Console.WriteLine("do you wanna continiu (y/n)");
                            if (Console.ReadLine() == "n")
                            {
                                stopper02 = true;
                            }
                            Console.Clear();
                        }
                        break;
                    default:
                        break;
                }
                Console.Clear();
            }
        }
    }
}
