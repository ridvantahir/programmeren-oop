﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _1._Keuzemenuutje_voor_latere_programmas
{
    public class Car
    {
        private double odometer;
        public double Odometer 
        { 
            get { return odometer; }
            set { odometer = value; }
        }
        private double speed = 60;
        public double Speed 
        { 
            get { return speed; }
            set
            {
                if (value >= 0 && value <= 120)
                {
                    speed = value;
                }
            }
        }

        /// <summary>
        /// accelerates the car
        /// </summary>
        public void Gas()
        {
            double OldSpeed = Speed;
            Speed += 10d;
            Odometer += ((OldSpeed + Speed) / 2) / 60;
        }

        /// <summary>
        /// slows down the car
        /// </summary>
        public void Brake()
        {
            double OldSpeed = Speed;
            Speed -= 10d;
            Odometer += ((OldSpeed - Speed) / 2) / 60;
        }
    }
}
