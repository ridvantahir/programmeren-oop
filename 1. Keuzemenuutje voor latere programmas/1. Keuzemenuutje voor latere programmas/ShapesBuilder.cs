﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _1._Keuzemenuutje_voor_latere_programmas
{
    public class ShapesBuilder
    {
        /// <summary>
        /// duh
        /// </summary>
        private ConsoleColor color;
        public ConsoleColor Color
        {
            get { return color; }
            set
            {
                color = value;
                Console.ForegroundColor = color;
            }
        }


        /// <summary>
        /// draws a triangle on the console
        /// </summary>
        /// <param name="heigth">give the heigth of the rectangle</param>
        /// <param name="width">give the width of the rectangle</param>
        public void Triangle(int heigth, int width)
        {
            //rows of the triangle
            for (int i = 0; i < heigth; i++)
            {
                // empty space of the triangle
                for (int j = i; j < width; j++)
                {
                    Console.Write(" ");
                }
                //stars that forms the triangle
                for (int y = 0; y <= i * 2; y++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// draws a triangle on the console with a alternative sign
        /// </summary>
        /// <param name="heigth">give the heigth of the rectangle</param>
        /// <param name="width">give the width of the rectangle</param>
        public void Triangle(int heigth, int width, string aternative)
        {
            //rows of the triangle
            for (int i = 0; i < heigth; i++)
            {
                // empty space of the triangle
                for (int j = i; j < width; j++)
                {
                    Console.Write(" ");
                }
                //stars that forms the triangle
                for (int y = 0; y <= i * 2; y++)
                {
                    Console.Write(aternative);
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// draws a triangle on the console with a set color
        /// </summary>
        /// <param name="heigth">give the heigth of the rectangle</param>
        /// <param name="width">give the width of the rectangle</param>
        public void Triangle(int heigth, int width, ConsoleColor color)
        {
            this.color = color;

            //rows of the triangle
            for (int i = 0; i < heigth; i++)
            {
                // empty space of the triangle
                for (int j = i; j < width; j++)
                {
                    Console.Write(" ");
                }
                //stars that forms the triangle
                for (int y = 0; y <= i * 2; y++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// draws a triangle on the console with a alternative sign and a set color
        /// </summary>
        /// <param name="heigth">give the heigth of the rectangle</param>
        /// <param name="width">give the width of the rectangle</param>
        public void Triangle(int heigth, int width, string aternative, ConsoleColor color)
        {
            this.color = color;

            //rows of the triangle
            for (int i = 0; i < heigth; i++)
            {
                // empty space of the triangle
                for (int j = i; j < width; j++)
                {
                    Console.Write(" ");
                }
                //stars that forms the triangle
                for (int y = 0; y <= i * 2; y++)
                {
                    Console.Write(aternative);
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Draws a rectangle on the console
        /// </summary>
        /// <param name="heigth">give the heigth of the rectangle</param>
        /// <param name="width">give the width of the rectangle</param>
        public void Rectangle(int heigth, int width)
        {
            //for the height of the rectangle
            for (int i = 0; i < heigth; i++)
            {
                //for the width of the rectangle
                for (int j = 0; j < width; j++)
                {
                    Console.Write('*');
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Draws a rectangle on the console with alternative sign
        /// </summary>
        /// <param name="heigth">give the heigth of the rectangle</param>
        /// <param name="width">give the width of the rectangle</param>
        public void Rectangle(int heigth, int width, string aternative)
        {
            //for the height of the rectangle
            for (int i = 0; i < heigth; i++)
            {
                //for the width of the rectangle
                for (int j = 0; j < width; j++)
                {
                    Console.Write(aternative);
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Draws a rectangle on the console a set color
        /// </summary>
        /// <param name="heigth">give the heigth of the rectangle</param>
        /// <param name="width">give the width of the rectangle</param>
        public void Rectangle(int heigth, int width, ConsoleColor color)
        {
            this.color = color;

            //for the height of the rectangle
            for (int i = 0; i < heigth; i++)
            {
                //for the width of the rectangle
                for (int j = 0; j < width; j++)
                {
                    Console.Write('*');
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Draws a rectangle on the console with alternative sign and a set color
        /// </summary>
        /// <param name="heigth">give the heigth of the rectangle</param>
        /// <param name="width">give the width of the rectangle</param>
        public void Rectangle(int heigth, int width, string aternative, ConsoleColor color)
        {
            this.color = color;

            //for the height of the rectangle
            for (int i = 0; i < heigth; i++)
            {
                //for the width of the rectangle
                for (int j = 0; j < width; j++)
                {
                    Console.Write(aternative);
                }
                Console.WriteLine();
            }
        }
    }
}
